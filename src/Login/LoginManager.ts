import {LoginAPI} from '@/Api';

export default class LoginManager {

    public static setActiveUser(userId: number, login: string, token: string): void {
        localStorage.setItem('login', login);
        localStorage.setItem('token', token);
        localStorage.setItem('userId', userId.toString());
    }

    public static getUserId(): number {
        const userIdString = localStorage.getItem('userId');
        if (userIdString === null) {
            throw new Error('nobody is logged at the moment');
        }
        return Number.parseFloat(userIdString);
    }

    public static getLogin(): string {
        const login = localStorage.getItem('login');
        if (login === null) {
            throw new Error('nobody is logged at the moment');
        }
        return login;
    }

    public static isLogged(): boolean {
        return localStorage.getItem('login') !== null;
    }

    public static logout(): void {
        const token = this.getToken();
        const userId = this.getUserId();
        this.removeLocalStorageItems();
        new LoginAPI().logout(userId, token);
    }

    public static getToken(): string {
        const token = localStorage.getItem('token');
        if (token === null) {
            throw new Error('nobody is logged at the moment');
        }
        return token;
    }

    private static removeLocalStorageItems(): void {
        localStorage.removeItem('login');
        localStorage.removeItem('userId');
        localStorage.removeItem('token');
    }
}

