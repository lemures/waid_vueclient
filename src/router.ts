import Vue from 'vue';
import VueRouter from 'vue-router';
import SetPasswordView from '@/Login/SetPasswordView.vue';
import DailyHabitsList from '@/MainPage/ToDoThingsList.vue';
import LoginView from '@/Login/LoginView.vue';
import FinancesView from '@/Finances/FinancesView.vue';
import DevicesView from '@/Devices/DevicesView.vue';
import RunningView from '@/Running/RunningView.vue';
import ApiKeysView from '@/ApiKeys/ApiKeysView.vue';

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/login',
      component: LoginView,
    },
    {
      path: '/password/:userId',
      component: SetPasswordView,
    },
    {
      path: '/',
      component: DailyHabitsList,
    },
    {
      path: '/finances',
      component: FinancesView
    },
    {
      path: '/devices',
      component: DevicesView
    },
    {
      path: '/running',
      component: RunningView
    },
    {
      path: '/keys',
      component: ApiKeysView
    }
  ],
});
