import {FinancialRecordDto} from '@/Api';

export default class ApexChartsUtils {

    public static categoryLabel(record: FinancialRecordDto): string {
        return record.entries[0].date.year + '/'
            + (record.entries[0].date.month < 10 ? '0' : '')
            + record.entries[0].date.month;
    }

    public static xLabels(records: FinancialRecordDto[]): string[] {
        const result = [] as string[];
        for (const record of records) {
            if (result.indexOf(this.categoryLabel(record)) === -1) {
                result.push(this.categoryLabel(record));
            }
        }
        return result.sort();
    }

    public static seriesNames(records: FinancialRecordDto[], mergeSubcategories: boolean): string[] {
        const result = [] as string[];
        for (const record of records) {
            if (result.indexOf(this.getCategoryString(record.entries[0].category, mergeSubcategories)) === -1) {
                result.push(this.getCategoryString(record.entries[0].category, mergeSubcategories));
            }
        }
        return result;
    }

    public static generateSumSeries(records: FinancialRecordDto[], xLabels: string[]): any[] {
        const series = [];
        series.push({
            name: 'Sum',
            data: new Array(xLabels.length).fill(0)
        });

        for (const record of records) {
            const category = this.categoryLabel(record);
            const index = xLabels.indexOf(category);

            const foundSeries = series[0];
            if (foundSeries === undefined) {
                continue;
            }
            foundSeries.data[index] += record.entries[0].value;
        }

        for (const singleSeries of series) {
            for (let index = 0; index < singleSeries.data.length; index++) {
                singleSeries.data[index] = singleSeries.data[index].toFixed(2);
            }
        }
        return series;
    }

    public static generateSeries(records: FinancialRecordDto[], categories: string[], xLabels: string[], mergeSubcategories: boolean): Series[] {
        const series = [];
        for (const label of categories) {
            series.push({
                name: label,
                data: new Array(xLabels.length).fill(0)
            });
        }
        for (const record of records) {
            const categoryString = this.getCategoryString(record.entries[0].category, mergeSubcategories);
            const foundSeries = series.find((x) => x.name === categoryString);
            if (foundSeries === undefined) {
                continue;
            }
            const index = xLabels.indexOf(this.categoryLabel(record));
            foundSeries.data[index] = foundSeries.data[index] + record.entries[0].value;
        }

        for (const singleSeries of series) {
            for (let index = 0; index < singleSeries.data.length; index++) {
            singleSeries.data[index] = singleSeries.data[index].toFixed(2);
            }
        }
        return series;
    }

    public static averageSeries(series: Series[] , monthsToAverage: number): Series[] {
        const result = [];
        for (const oneSeries of series) {
            const toAdd: Series = {name: oneSeries.name, data: []};
            if (oneSeries.data.length < monthsToAverage) {
                toAdd.data = oneSeries.data;
            } else {
                let accumulated = 0;
                for (let i = 0; i < monthsToAverage - 1 ; i++) {
                    accumulated += parseFloat(oneSeries.data[i]);
                }
                for (let i = monthsToAverage - 1 ; i < oneSeries.data.length; i++) {
                    accumulated += parseFloat(oneSeries.data[i]);
                    toAdd.data.push((accumulated / monthsToAverage).toFixed(2));
                    accumulated -= parseFloat(oneSeries.data[i - monthsToAverage + 1]);
                }
            }
            result.push(toAdd);
        }
        return result;
    }

    private static getCategoryString(category: string[], mergeSubcategories: boolean = false): string {
        if (category.length === 0 || category[0] === '') {
            return 'Without category';
        }
        if (mergeSubcategories) {
            return category[0];
        }
        let result = '';
        for (let i = 0; i < category.length - 1; i++) {
            result += category[i];
            result += ' / ';
        }
        result += category[category.length - 1];
        return result;
    }
}

interface Series {
 name: string;
 data: string[];
}
