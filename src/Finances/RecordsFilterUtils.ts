import ApexChartsUtils from '@/Finances/ApexChartsUtils';
import {FinancialRecordDto} from '@/Api';
import FinancialCategoriesUtils from '@/Finances/FinancialCategoriesUtils';

export default class RecordsFilterUtils {

    public static filteredRecords(records: FinancialRecordDto[],
                                  search: string,
                                  chosenTypes: string[],
                                  chosenCategories: string[][],
                                  chosenMonths: string[]): FinancialRecordDto[] {
        let result = [];
        for (const record of records) {
            for (const category of chosenCategories) {
                if (this.categoriesSame(category, record.entries[0].category)) {
                    result.push(record);
                    break;
                }
            }
        }

        result = result.filter((x) => {
            const category = ApexChartsUtils.categoryLabel(x);
            return chosenMonths.indexOf(category) !== -1;
        });

        if (search !== '') {
            result = result.filter((x) =>
                x.transactionData.name.toLowerCase().indexOf(search.toLowerCase()) !== -1 ||
                x.entries[0].value.toFixed(2).indexOf(search.toLowerCase()) !== -1 ||
                x.entries[0].description.toLowerCase().indexOf(search.toLowerCase()) !== -1);
        }

        result = result.filter((record) => chosenTypes.includes(FinancialCategoriesUtils.extractTransactionType(record)));

        let uniqueId = 0;
        for (const record of result) {
            (record as any).uniqueId = uniqueId;
            uniqueId++;
        }
        return result;
    }

    public static categoriesSame(pattern: string[], category: string[]) {
        if (pattern.length !== category.length) {
            return false;
        }
        for (let i = 0; i < pattern.length; i++) {
            if (pattern[i] !== category[i]) {
                return false;
            }
        }
        return true;
    }

    public static categoriesMatch(pattern: string[], category: string[]) {
        if (pattern.length > category.length || pattern.length === 0) {
            return false;
        }
        for (let i = 0; i < pattern.length; i++) {
            if (pattern[i] !== category[i]) {
                return false;
            }
        }
        return true;
    }

    public static anyCategoryMatch(patterns: string[][], category: string[]) {
        return patterns.find((x) => this.categoriesMatch(x, category)) !== undefined;
    }
}
