import {FinancialCategoryDto, FinancialRecordDto} from '@/Api';

export default class FinancialCategoriesUtils {

    public static computeAllCategories(categories: FinancialCategoryDto[]): string[][] {
        const result = [];
        for (const category of categories) {
            const toAdd = [category.name];
            const childCategories = this.computeAllCategories(category.children);
            result.push(toAdd);
            for (const childCategory of childCategories) {
                result.push(toAdd.concat(childCategory));
            }
        }
        return result;
    }

    public static computeAllTransactionTypes(records: FinancialRecordDto[]): string[] {
        return [...new Set(records.map((x) => this.extractTransactionType(x)))];
    }

    public static extractTransactionType(record: FinancialRecordDto): string {
        const accountNumber = record.transactionData.value < 0 ?
            record.transactionData.sourceNumber :
            record.transactionData.destinationNumber;
        if (record.transactionData.name.includes('0946')) {
            return 'Karta Tomek';
        }
        if (record.transactionData.name.includes('2666')) {
            return 'Karta Ola';
        }
        if (record.transactionData.name.includes('2845')) {
            return 'Karta Ola';
        }
        if (accountNumber === null || accountNumber === '') {
            return record.transactionData.value < 0 ? 'Przelew wychodzący' : 'Przelew przychodzący';
        }
        if (accountNumber.trim().endsWith('8226')) {
            return 'Stare konto Ola';
        }
        if (accountNumber.trim().endsWith('6545')) {
            return record.transactionData.value < 0 ? 'Przelew wychodzący' : 'Przelew przychodzący';
        }
        return 'unknown';
    }
}
