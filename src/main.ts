import Vue from 'vue';
import Vuetify from 'vuetify';
import App from './App.vue';
import router from './router';
import '@mdi/font/css/materialdesignicons.css';
import 'vuetify/dist/vuetify.min.css';
import ThemeUtils from '@/Utils/ThemeUtils';

import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import 'quill/dist/quill.bubble.css';

// @ts-ignore
import VueQuillEditor from 'vue-quill-editor';

import 'codemirror/mode/javascript/javascript.js';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/monokai.css';

// @ts-ignore
import VueCodemirror from 'vue-codemirror';

// @ts-ignore
import VueQrcodeReader from "vue-qrcode-reader";

ThemeUtils.init();

Vue.use(VueCodemirror);
Vue.use(Vuetify);
Vue.use(VueQuillEditor);
Vue.use(VueQrcodeReader);

Vue.config.productionTip = false;

const vuetify = ThemeUtils.vuetifyInstance();

new Vue({
    router,
    vuetify,
    render: (h) => h(App),
}).$mount('#app');
