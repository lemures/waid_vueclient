export enum ThingType {
    NotYetSpecified,
    DailyRoutine,
    OneTime,
    SpecificDaysInWeek,
    EveryNDays,
    Empty,
    NewEveryNthDayOfMonth
}
