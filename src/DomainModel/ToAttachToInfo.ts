import {ThingIdsDto} from '@/Api';

export interface ToAttachToInfo {
    thingIds: ThingIdsDto;

    name: string;
}
