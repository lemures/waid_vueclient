export enum ModalType {
    StringModal,
    DateModal,
    NumberModal,
    HourModal,
    SelectModal,
    DateTimeModal
}
