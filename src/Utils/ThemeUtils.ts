import Vuetify from 'vuetify';
import {VuetifyPreset} from 'vuetify/types/presets';
import colors from 'vuetify/lib/util/colors';

export default class ThemeUtils {

    public static init(): void {
        if (localStorage.getItem('theme') !== null) {
            this.isThemeLight = true;
        }
        this.vuetify = new Vuetify({
            theme: {
                themes: this.isThemeLight ? this.light() : this.dark()
            },
        });
    }

    public static vuetifyInstance(): VuetifyPreset {
        return this.vuetify;
    }

    public static switchTheme(vueInstance: any): void {
        this.isThemeLight = !this.isThemeLight;
        localStorage.setItem('theme', this.isThemeLight ? 'light' : 'dark');
        const theme = this.isThemeLight ? this.light() : this.dark();
        if (theme.light === undefined) {
            return;
        }
        for (const color of Object.keys(theme.light)) {
            vueInstance.$vuetify.theme.themes.light[color] = (theme.light as any)[color];
        }
    }

    public static isLight(): boolean {
        return this.isThemeLight;
    }

    private static vuetify: VuetifyPreset;

    private static isThemeLight: boolean = true;

    private static light() {
        return {
            light: {
                primary: colors.lightBlue.lighten2,
                primaryVariant: colors.lightBlue.lighten1,
                secondary: colors.deepPurple.lighten2,
                secondaryVariant: colors.deepPurple.lighten1,
                background: colors.grey.lighten4,
                surface: colors.grey.lighten5,
                error: colors.deepPurple.lighten1,
            }
        };
    }

    private static dark() {
        return {
            light: {
                primary: colors.deepOrange.lighten1,
                primaryVariant: colors.deepOrange.lighten2,
                secondary: colors.grey.darken2,
                secondaryVariant: colors.grey.darken3,
                background: colors.grey.darken4,
                surface: colors.grey.darken3,
                error: colors.deepOrange.darken1
            }
        };
    }
}
