import {ThingIdsDto, ToDoThingGeneralInfo} from '@/Api';

export default class ThingUtils {

    public static findById(habits: ToDoThingGeneralInfo[], ids: ThingIdsDto): ToDoThingGeneralInfo {
        let found = habits.find((x) => x.thingIds.thingId === ids.thingId);
        if (found === undefined) {
            found = new ToDoThingGeneralInfo();
        }
        found = this.findAmongChildren(ids, found);
        if (found === undefined) {
            found = new ToDoThingGeneralInfo();
        }
        return found;
    }

    private static findAmongChildren(ids: ThingIdsDto, thing: ToDoThingGeneralInfo): ToDoThingGeneralInfo | undefined {
        if (thing.thingIds.subThingId === ids.subThingId) {
            return thing;
        }
        const resultParent = thing.children.find((x) => this.findAmongChildren(ids, x) !== undefined);
        if (resultParent === undefined) {
            return undefined;
        }
        return this.findAmongChildren(ids, resultParent);
    }
}
