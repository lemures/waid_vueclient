export default class PortabilityUtils {

    public static isMobile(): boolean {
        return window.innerWidth < 800;
    }

}
