import {DateDto, DateTimeDto, HourDto} from '@/Api';

export default class TimeUtils {

    public static formatHourDto(value: HourDto): string {
        if (value.hour === null) {
            return '';
        }
        return (value.hour < 10 ? '0' : '') + value.hour + ':' + (value.minute < 10 ? '0' : '') + value.minute;
    }

    public static formatDate(value: Date): string {
        return value.getFullYear()
            + '-' + ((value.getMonth() + 1) < 10 ? '0' : '') + (value.getMonth() + 1)
            + '-' + (value.getDate() < 10 ? '0' : '') + value.getDate();
    }

    public static formatDateDto(value: DateDto): string {
        if (!value) {
            return '';
        }
        return value.year
            + '-' + ((value.month) < 10 ? '0' : '') + (value.month)
            + '-' + (value.day < 10 ? '0' : '') + value.day;
    }

    public static formatDateTimeDto(value: DateTimeDto): string {
        return this.formatDateDto(value.date) + ' ' + this.formatHourDto(value.hour);
    }

    public static isInPast(value: DateTimeDto): boolean {
        const now = new Date(Date.now());
        const date = new Date(
            value.date.year,
            value.date.month - 1,
            value.date.day,
            value.hour.hour,
            value.hour.minute,
            0);
        return date < now;
    }

    public static isInLast30Days(value: DateDto): boolean {
        const thirtyDaysInPast = new Date(Date.now());
        thirtyDaysInPast.setDate(thirtyDaysInPast.getDate() - 30);
        const date = new Date(
            value.year,
            value.month - 1,
            value.day,
            0,
            0,
            0);
        return date > thirtyDaysInPast;
    }
}
