export default class WebAddressUtils {

    public static getAddress(): string {
        return 'webpackHotUpdate' in window ?  'http://localhost:5100' : 'http://waid.pl';
    }

}
