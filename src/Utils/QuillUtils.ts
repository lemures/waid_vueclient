import {CommentDto, DateDto} from '@/Api';

export default class QuillUtils {

    public static divideIntoComments(quillText: string): CommentDto[] {
        const result: CommentDto[] = [] ;
        const lines = [];

        let currentLine = '';
        let tagDepth = 0;
        let insideTag = false;
        let closingTag = false;
        let tagValue = '';

        for (let i = 0; i < quillText.length; i++) {
            const letter = quillText[i];
            if (letter === '<') {
                insideTag = true;
                tagValue = '';
            }

            if (insideTag && letter === '>') {
                if (tagValue !== 'br') {
                    if (closingTag) {
                        tagDepth --;
                    } else {
                        tagDepth ++;
                    }
                }

                insideTag = false;
                closingTag = false;
            }

            if (insideTag && letter === '/' && quillText[i - 1] === '<') {
                closingTag = true;
            }

            currentLine += letter;

            if (insideTag && letter !== '<') {
                tagValue += letter;
            }

            if (letter === '>' && tagDepth === 0) {
                lines.push(currentLine);
                currentLine = '';
            }
        }
        if (currentLine !== '') {
            lines.push(currentLine);
        }

        for (const line of lines) {
            const comment = new CommentDto();
            comment.date = new DateDto({year: 2020, month: 1, day: 1});
            comment.value = line;
            result.push(comment);
        }

        return result;
    }

    public static convertLegacyComment(commentValue: string): string {
        if (commentValue.length === 0 || commentValue[0] !== '<') {
            return '<p>' + commentValue + '</p>';
        }
        return commentValue;
    }
}
