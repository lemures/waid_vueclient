export default class TransitionUtils {

    public static collapseSection(element: any) {
        const sectionHeight = element.scrollHeight;
        const duration = element.style.transitionDuration;
        const elementTransition = element.style.transition;
        element.style.transition = '';

        requestAnimationFrame(() => {
            element.style.height = sectionHeight + 'px';
            element.style.transition = elementTransition;
            element.style.transitionDuration = duration;
            requestAnimationFrame(() => {
                element.style.height = 0 + 'px';
            });
        });
        element.setAttribute('data-collapsed', 'true');
    }

    public static expandSection(element: any) {
        const sectionHeight = element.scrollHeight;
        element.style.height = sectionHeight + 'px';

        const removeListener = () => {
            element.removeEventListener('transitionend', removeListener);
            element.style.height = null;
        };
        element.addEventListener('transitionend', removeListener);
        element.setAttribute('data-collapsed', 'false');
    }

    public static perform(name: string, ctx: any, animationTime: number) {

        if (ctx.collapsiblesVisible[name] === undefined) {
            ctx.$set(ctx.collapsiblesVisible, name, 'a');
        } else {
            window.setTimeout(() => ctx.$set(ctx.collapsiblesVisible, name, undefined), animationTime * 1000);
        }
        ctx.$nextTick(() => {
            const section = document.querySelector('#collapsible' + name);
            if (section === null) {
                return;
            }
            const isCollapsed = section.getAttribute('data-collapsed') !== 'false';
            (section as any).style.transitionDuration = animationTime + 's';

            if (isCollapsed) {
                TransitionUtils.expandSection(section);
                section.setAttribute('data-collapsed', 'false');
            } else {
                TransitionUtils.collapseSection(section);
            }
        });
    }

}
