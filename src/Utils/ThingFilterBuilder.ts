import {ToDoThingGeneralInfo} from '@/Api';

export default class ThingFilterBuilder {

    public static all(): ThingFilterBuilder {
        return new ThingFilterBuilder();
    }

    private limit: number | undefined = undefined;

    private includeArchivedValue: boolean = false;

    private includeCompletedValue: boolean = false;

    private stringFilter: string | undefined = undefined;

    private groupFilter: string | undefined = undefined;

    private onlyWithoutGroup: boolean = false;

    public limitFirst(n: number): ThingFilterBuilder {
        this.limit = n;
        return this;
    }

    public includeCompleted(value: boolean): ThingFilterBuilder {
        this.includeCompletedValue = value;
        return this;
    }

    public includeArchived(value: boolean): ThingFilterBuilder {
        this.includeArchivedValue = value;
        return this;
    }

    public includeStringFilter(value: string): ThingFilterBuilder {
        this.stringFilter = value;
        return this;
    }

    public includeOnlyFromGroup(groupName: string): ThingFilterBuilder {
        this.groupFilter = groupName;
        return this;
    }

    public withoutGroup(): ThingFilterBuilder {
        this.onlyWithoutGroup = true;
        return this;
    }

    public filter(things: ToDoThingGeneralInfo[]): ToDoThingGeneralInfo[] {
        let result = things;
        if (this.onlyWithoutGroup) {
            result = result.filter((x) => !x.groupName);
        }
        if (this.groupFilter) {
            result = result.filter((x) => x.groupName === this.groupFilter);
        }
        if (!this.includeArchivedValue) {
            result = result.filter((x) => !x.isArchived);
        }
        if (!this.includeCompletedValue) {
            result = result.filter((x) => !x.isCompleted);
        }
        result = result.filter((x) => this.matchesAnyChild(x));

        if (this.limit !== undefined) {
            result = result.reverse();
            result = result.slice(0, this.limit);
        }

        return result;
    }

    private matchesSearchFilter(thing: ToDoThingGeneralInfo): boolean {
        if (this.stringFilter === undefined) {
            return true;
        }
        return thing.name.toLowerCase().includes(this.stringFilter.toLowerCase()) ||
            (thing.groupName !== null && thing.groupName.toLowerCase().includes(this.stringFilter.toLowerCase()));
    }


    private matchesAnyChild(thing: ToDoThingGeneralInfo): boolean {
        if (this.matchesSearchFilter(thing)) {
            return true;
        }
        return thing.children.find((x) => this.matchesAnyChild(x)) !== undefined;
    }
}
