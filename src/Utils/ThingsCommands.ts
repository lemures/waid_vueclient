import {
    AbstractCommand, AddCommentCommand,
    ArchiveCommand, AssignToGroupCommand,
    CancelSuspend,
    DateDto,
    DateTimeDto,
    DisableForDaysCommand,
    DismissCommand,
    HourDto,
    IAbstractCommand,
    MoveToOtherDateCommand,
    MoveToOtherHourCommand,
    ReEnableCommand,
    RemoveNotificationsCommand,
    RenameCommand, SetHoursToExpireCommand,
    ThingIdsDto,
    ToDoThingGeneralInfo,
    ToDoThingsAPI, UpdateOffsetCommand
} from '@/Api';
import LoginManager from '@/Login/LoginManager';

export default class ThingsCommands {

    private readonly api: ToDoThingsAPI = new ToDoThingsAPI();
    private readonly habits: ToDoThingGeneralInfo[] = [];
    private editedHabit: ToDoThingGeneralInfo = new ToDoThingGeneralInfo();

    public constructor(habits: ToDoThingGeneralInfo[]) {
        this.habits = habits;
    }

    public setEditedHabit(habit: ToDoThingGeneralInfo) {
        this.editedHabit = habit;
    }

    public attachSubThing(whatToAttach: ThingIdsDto): void {
        const habit = this.editedHabit;
        this.api.attachSubThing(habit.userId,
            habit.thingIds.thingId,
            habit.thingIds.subThingId,
            whatToAttach)
            .then(() => {
                this.updated(habit.thingIds);
                if (whatToAttach.thingId === whatToAttach.subThingId) {
                    this.deleted(whatToAttach);
                } else {
                    this.updated(whatToAttach);
                }
            });
    }

    public detach() {
        const thing = this.editedHabit;
        this.api.detachSubThing(thing.userId, thing.thingIds)
            .then(() => {
                this.updated(thing.thingIds);
                const newIds = new ThingIdsDto();
                newIds.thingId = thing.thingIds.subThingId;
                newIds.subThingId = thing.thingIds.subThingId;
                this.created(newIds);
            });
    }

    public rename(renameValue: string) {
        const command = new RenameCommand({newName: renameValue});
        this.executeCommandUpdatingThing(command);
    }

    public deleteRoutine(): void {
        this.api.removeThing(this.editedHabit.userId, this.editedHabit.thingIds)
            .then(() => this.deleted(this.editedHabit.thingIds));
    }

    public archive() {
        this.executeCommandUpdatingThing(new ArchiveCommand());
    }

    public reEnable() {
        this.executeCommandUpdatingThing(new ReEnableCommand());
    }

    public addComment(commentValue: string): void {
        const split = commentValue.split('\n');
        for (const value of split) {
            const date = new DateDto();
            date.year = new Date(Date.now()).getFullYear();
            date.month = new Date(Date.now()).getMonth();
            date.day = new Date(Date.now()).getDay();
            this.executeCommandUpdatingThing(new AddCommentCommand({date, value}));
        }
    }

    public assignToGroup(groupName: string): void {
        const command = new AssignToGroupCommand();
        command.groupName = groupName;
        this.executeCommandUpdatingThing(command);
    }

    public disableForDays(amount: number) {
        const command = new DisableForDaysCommand({amount});
        this.executeCommandUpdatingThing(command);
    }

    public moveToOtherDate(data: DateTimeDto): void {
        const command = new MoveToOtherDateCommand({date: data.date, hour: data.hour});
        this.executeCommandUpdatingThing(command);
    }

    public moveToOtherHour(hour: HourDto) {
        const command = new MoveToOtherHourCommand({hour});
        this.executeCommandUpdatingThing(command);
    }

    public cancelSuspend() {
        this.executeCommandUpdatingThing(new CancelSuspend());
    }

    public dismiss() {
        this.executeCommandUpdatingThing(new DismissCommand());
    }


    public setHoursToExpire(value: number) {
        this.executeCommandUpdatingThing(new SetHoursToExpireCommand({value}));
    }

    public removeNotifications() {
        this.executeCommandUpdatingThing(new RemoveNotificationsCommand());
    }

    public updateOffset(offsetValue: number) {
        this.executeCommandUpdatingThing(new UpdateOffsetCommand({offsetValue}));
    }

    public created(thingId: ThingIdsDto) {
        this.api.getThing(LoginManager.getUserId(), thingId.thingId)
            .then((response) => this.habits.push(response));
    }

    public deleted(thingId: ThingIdsDto) {
        const index = this.habits.findIndex((x) => x.thingIds.thingId === thingId.thingId);
        if (this.habits[index].thingIds.subThingId === thingId.subThingId) {
            this.habits.splice(index, 1);
        } else {
            this.deleteRecursive(this.habits[index], thingId);
        }
    }

    public updated(thingId: ThingIdsDto) {
        this.api.getThing(LoginManager.getUserId(), thingId.thingId).then((response) => {
            const index = this.habits.findIndex((x) => x.thingIds.thingId === thingId.thingId);
            this.habits.splice(index, 1, response);
        });
    }

    private deleteRecursive(thing: ToDoThingGeneralInfo, thingId: ThingIdsDto) {
        thing.children = thing.children.filter((x) => x.thingIds.subThingId !== thingId.subThingId);
        for (const child of thing.children) {
            this.deleteRecursive(child, thingId);
        }
    }

    private executeCommandUpdatingThing(command: AbstractCommand) {
        this.executeCommand(command, () => this.updated(this.editedHabit.thingIds));
    }

    private executeCommand(command: AbstractCommand, action: () => void) {
        const thing = this.editedHabit;
        this.api.executeCommand(thing.userId, thing.thingIds.thingId, thing.thingIds.subThingId, command)
            .then(() => action());
    }
}
