import {
    AbstractType,
    AbstractValue,
    BoolType,
    BoolValue,
    FloatType, FloatValue,
    IntType, IntValue,
    StringType,
    StringValue,
    VoidType, VoidValue
} from '@/Api';

export default class DevicesApiUtils {

    public static typeToString(type: AbstractType): string {
        if (type instanceof IntType) {
            return 'int';
        }
        if (type instanceof FloatType) {
            return 'float';
        }
        if (type instanceof StringType) {
            return 'string';
        }
        if (type instanceof BoolType) {
            return 'bool';
        }
        if (type instanceof VoidType) {
            return 'void';
        }
        throw Error(`Unknown type ${type}`);
    }

    public static objectToAbstractValue(value: any, expectedType: AbstractType): AbstractValue {
        if (this.typeToString(expectedType) === 'string') {
            const result = new StringValue();
            result.value = value;
            return result;
        }
        if (this.typeToString(expectedType) === 'bool') {
            const result = new BoolValue();
            result.value = typeof value === 'boolean' ? value : (value === 'true');
            return result;
        }
        if (this.typeToString(expectedType) === 'int') {
            const result = new IntValue();
            result.value = typeof value === 'number' ? value : Number.parseInt(value, 10);
            return result;
        }
        if (this.typeToString(expectedType) === 'float') {
            const result = new FloatValue();
            result.value = typeof value === 'number' ? value : Number.parseFloat(value);
            return result;
        }
        throw Error(`unable to convert ${JSON.stringify(value)} of type ${typeof value}`
            + `to AbstractValue of type ${JSON.stringify(expectedType)}`);
    }

    public static abstractValueToObject(value: AbstractValue): any {
        if (value instanceof IntValue) {
            return value.value;
        }
        if (value instanceof FloatValue) {
            return value.value;
        }
        if (value instanceof BoolValue) {
            return value.value;
        }
        if (value instanceof StringValue) {
            return value.value;
        }
        if (value instanceof VoidValue) {
            return undefined;
        }
        throw Error(`unable to convert ${JSON.stringify(value)} to javascript object`);
    }
}
